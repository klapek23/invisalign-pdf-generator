const concat = require('concat');

const files = ['./dist/invisalign/runtime.js', './dist/invisalign/polyfills.js', './dist/invisalign/main.js'];

(async function build() {
  await concat(files, './dist/invisalign/pdf-generator.js');
})();
