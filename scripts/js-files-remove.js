const fs = require('fs-extra');

const files = ['./dist/invisalign/runtime.js', './dist/invisalign/polyfills.js', './dist/invisalign/main.js', './invisalign/dist/vendor.js'];

(async function remove() {
  await Promise.all(files.map((file) => fs.remove(file)));
})();
