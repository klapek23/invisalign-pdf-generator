const fs = require('fs-extra');
const cheerio = require('cheerio');

const indexFilePath = `${__dirname}/../dist/invisalign/index.html`,
  scriptToInsertPath = 'pdf-generator.js',
  scriptsToRemove = ['runtime.js', 'polyfills.js', 'main.js'];

(async function switchScripts() {
  const indexSource = await fs.readFile(indexFilePath, 'utf8');
  const $ = cheerio.load(indexSource);
  $('script')
    .filter((index, { attribs }) => scriptsToRemove.includes(attribs.src))
    .remove();
  $('body').append(`<script type="text/javascript" src="${scriptToInsertPath}"></script>`);
  await fs.writeFile(indexFilePath, $.html(), 'utf8');
})();
