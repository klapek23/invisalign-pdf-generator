import { NgModule } from '@angular/core';
import {CommonModule, CurrencyPipe, DatePipe} from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';

import { InvisalignPaymentsComponent } from './invisalign-payments/invisalign-payments.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatCardModule,
    MatDividerModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatSelectModule,
    MatIconModule,
  ],
  exports: [InvisalignPaymentsComponent],
  declarations: [InvisalignPaymentsComponent],
  providers: [DatePipe, CurrencyPipe]
})
export class InvisalignPaymentsModule { }
