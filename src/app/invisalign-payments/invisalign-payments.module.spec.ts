import { InvisalignPaymentsModule } from './invisalign-payments.module';

describe('InvisalignPaymentsModule', () => {
  let invisalignPaymentsModule: InvisalignPaymentsModule;

  beforeEach(() => {
    invisalignPaymentsModule = new InvisalignPaymentsModule();
  });

  it('should create an instance', () => {
    expect(invisalignPaymentsModule).toBeTruthy();
  });
});
