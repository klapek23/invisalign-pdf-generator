import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import * as moment from 'moment';

import { PdfGenerator } from '../pdf-generator';
import { TreatmentTypeInterface, TreatmentTypes } from '../../types/TreatmentTypes';

@Component({
  selector: 'app-invisalign-payments',
  templateUrl: './invisalign-payments.component.html',
  styleUrls: ['./invisalign-payments.component.scss']
})
export class InvisalignPaymentsComponent {
  treatmentTypes = TreatmentTypes;
  invisalignPaymentsForm = new FormGroup({
    treatmentStartDate: new FormControl(new Date(), Validators.required),
    patientName: new FormControl('', Validators.required),
    treatmentType: new FormControl('', Validators.required),
    schedule: new FormGroup({}),
  });
  pdfGenerator = new PdfGenerator();
  scheduleItems: string[] = [];

  get patientName() {
    return this.invisalignPaymentsForm.get('patientName');
  }

  get treatmentStartDate() {
    return this.invisalignPaymentsForm.get('treatmentStartDate');
  }

  get treatmentType() {
    return this.invisalignPaymentsForm.get('treatmentType');
  }

  get amountOfAligners() {
    return this.invisalignPaymentsForm.get('amountOfAligners');
  }

  get amountOfAlingersValue() {
    return this.treatmentType!.value.type === 'comprehensive'
      ? this.amountOfAligners!.value
      : TreatmentTypes.find(({ type }: TreatmentTypeInterface) => type === this.treatmentType!.value.type)!.aligners;
  }

  get amountOfWeeksOfTreatmentValue() {
    return this.treatmentType!.value.type === 'comprehensive'
      ? this.amountOfAligners!.value + 6
      : TreatmentTypes.find(({ type }: TreatmentTypeInterface) => type === this.treatmentType!.value.type)!.weeks;
  }

  get priceOfCleancheckValue() {
    return TreatmentTypes.find(({ type }: TreatmentTypeInterface) => type === this.treatmentType!.value.type)!.priceOfCleancheck;
  }

  get amountOfDownpaymentValue() {
    return TreatmentTypes.find(({ type }: TreatmentTypeInterface) => type === this.treatmentType!.value.type)!.amountOfDownpayment;
  }

  get amountOfInstalmentsValue() {
    return TreatmentTypes.find(({ type }: TreatmentTypeInterface) => type === this.treatmentType!.value.type)!.amountOfInstalments;
  }

  get datesForAppointmentsAndPaymentSchedule() {
    const schedulesGroup = this.invisalignPaymentsForm.get('schedule') as FormGroup;
    return Object.keys(schedulesGroup.controls).map(
      (key: string) => {
        const { title, date, price } = (schedulesGroup.get(key) as FormControl).value;
        return `${title}: ${moment(date).format('DD/MM/YYYY')} - ${this.currencyPipe.transform(price, 'PLN')}`;
      }
    );
  }

  getScheduleItemFormControl(scheduleItem: string, name: string): FormControl {
    return ((this.invisalignPaymentsForm.get('schedule') as FormGroup).get(scheduleItem) as FormControl).get(name) as FormControl;
  }

  constructor(private currencyPipe: CurrencyPipe) {}

  addScheduleItem(title?: string, date?: Date, price?: string) {
    const schedulesGroup = this.invisalignPaymentsForm.get('schedule') as FormGroup;
    schedulesGroup.addControl(`schedule${this.scheduleItems.length}`, new FormGroup({
      title: new FormControl(title ? title : '', Validators.required),
      date: new FormControl(date ? date : '', Validators.required),
      price: new FormControl(price ? price : '', Validators.required),
    }));
    this.scheduleItems = [...this.scheduleItems, `schedule${this.scheduleItems.length}`];
  }

  removeScheduleItem(scheduleItem: string) {
    (this.invisalignPaymentsForm.get('schedule') as FormGroup).removeControl(scheduleItem);
    this.scheduleItems = this.scheduleItems.filter((item: string) => item !== scheduleItem);
  }

  handleTreatmentTypeChange() {
    this.treatmentType!.value.type === 'comprehensive'
      ? this._addAmountOfAlignersInput()
      : this._removeAmountOfAlignersInput();
    this._resetSchedules();
    this._generateSchedules();
  }

  handleAmountOfAlignersChange() {
    this._resetSchedules();
    this._generateSchedules();
  }

  handleFormSubmit(event: Event) {
    event.preventDefault();
    if (this.invisalignPaymentsForm.valid) {
      this.pdfGenerator.treatmentType = this.treatmentType!.value.displayName;
      this.pdfGenerator.patientName = this.patientName!.value;
      this.pdfGenerator.treatmentPrice = this.currencyPipe.transform(this.treatmentType!.value.price, 'PLN') as string;
      this.pdfGenerator.amountOfAligners = this.amountOfAlingersValue;
      this.pdfGenerator.amountOfInstalments = this.amountOfInstalmentsValue;
      this.pdfGenerator.amountOfWeeksOfTreatment = this.amountOfWeeksOfTreatmentValue;
      this.pdfGenerator.datesForAppointmentsAndPaymentSchedule = this.datesForAppointmentsAndPaymentSchedule;
      this.pdfGenerator.createPdf().open({ fileName: 'treatment' });
    }
  }

  private _generateSchedules() {
    setTimeout(() => {
      this.addScheduleItem(
        'Wizyta konsultacyjna',
        moment(this.treatmentStartDate!.value).toDate(),
        this.priceOfCleancheckValue.toString()
      );
      this.addScheduleItem(
        'Akceptacja planu leczenia i zaliczka',
        moment(this.treatmentStartDate!.value).add(2, 'weeks').toDate(),
        this.amountOfDownpaymentValue.toString()
      );
      this.treatmentType!.value.instalmentsSchedule.map(({ title, date, value }: any) => {
        this.addScheduleItem(
          title,
          moment(this.treatmentStartDate!.value).add(
            typeof date.amount === 'function' ? date.amount(this.amountOfAlingersValue) : date.amount,
            date.unit
          ).toDate(),
          value
        );
      });
    }, 0);
  }

  private _resetSchedules() {
    const schedulesGroup = this.invisalignPaymentsForm.get('schedule') as FormGroup;
    Object.keys(schedulesGroup.controls).map((key: string) => schedulesGroup.removeControl(key));
    this.scheduleItems = [];
  }

  private _addAmountOfAlignersInput() {
    this.invisalignPaymentsForm.addControl(
      'amountOfAligners',
      new FormControl(15, [Validators.required, Validators.min(15)])
    );
  }

  private _removeAmountOfAlignersInput() {
    this.invisalignPaymentsForm.removeControl('amountOfAligners');
  }

}
