import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvisalignPaymentsComponent } from './invisalign-payments.component';

describe('InvisalignPaymentsComponent', () => {
  let component: InvisalignPaymentsComponent;
  let fixture: ComponentFixture<InvisalignPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvisalignPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvisalignPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
