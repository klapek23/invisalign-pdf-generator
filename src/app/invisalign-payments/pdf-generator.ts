// @ts-ignore
import * as pdfMake from 'pdfmake/build/pdfmake';
// @ts-ignore
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

import { invisalignLogo } from './invisalign-logo';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

export class PdfGenerator {
  treatmentType?: string;
  patientName?: string;
  treatmentPrice?: string;
  amountOfInstalments?: number;
  amountOfAligners?: number;
  amountOfWeeksOfTreatment?: number;
  datesForAppointmentsAndPaymentSchedule: string[] = [];

  createPdf() {
    const docDefinition = {
      styles: {
        contact: {
          margin: [0, 10, 15, 0]
        },
        paragraph: {
          margin: [15, 5, 15, 5],
        },
        sign: {
          margin: [0, 50, 15, 0],
          width: 300,
          alignment: 'right'
        },
        slogan: {
          margin: [15, 25, 10, 25]
        },
        header: {
          fontSize: 14,
          bold: true,
          alignment: 'center',
          margin: [0, 40, 0, 40]
        }
      },
      content: [
        {
          columns: [
            {
              text: '"Twoi spejaliści od pięknego uśmiechu"',
              style: 'slogan'
            },
            [
              {
                image: invisalignLogo,
                width: 250,
                alignment: 'right'
              },
              {
                text: 'Kontakt: 887 05 05 01 \n biuro@dentestetica.pl',
                alignment: 'right',
                style: 'contact'
              },
            ],
          ]
        },
        {
          text: `KOSZTY LECZENIA METODĄ ${this.treatmentType!.toUpperCase()}`,
          style: 'header'
        },
        {
          type: 'patientName',
          text: `Imię i nazwisko pacjenta - ${this.patientName}.`,
          style: 'paragraph'
        },
        {
          type: 'treatmentPrice',
          text: `Koszt leczenia aparatem Invisalign wynosi ${this.treatmentPrice}.`,
          style: 'paragraph'
        },
        {
          type: 'amountOfAligners',
          text: `Liczba nakładek - ${this.amountOfAligners}`,
          style: 'paragraph'
        },
        {
          type: 'amountOfWeeksOfTreatment',
          text: `Długość leczenia (w tygodniach) - ${this.amountOfWeeksOfTreatment}`,
          style: 'paragraph'
        },
        {
          type: 'amountOfInstalments',
          text: `Ilość rat - ${this.amountOfInstalments}`,
          style: 'paragraph'
        },
        {
          text: `Pacjent zobowiązuje się do spłaty całkowitego kosztu leczenia według ustalonego harmonogramu płatności:`,
          style: 'paragraph'
        },
        {
          type: 'datesForAppointmentsAndPaymentSchedule',
          ul: this.datesForAppointmentsAndPaymentSchedule,
          style: 'paragraph'
        },
        {
          columns: [
            {
              width: '*',
              text: '',
            },
            {
              width: 350,
              text: 'Data i czytelny podpis pacjenta / rodzica / opiekuna prawnego \n\n ..................................................',
              style: 'sign'
            }
          ]
        }
      ],
    };
    return pdfMake.createPdf(docDefinition);
  }
}
