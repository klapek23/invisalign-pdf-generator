import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { SessionService } from '../../services/session/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required, SessionService.validateUsername]),
    password: new FormControl('', [Validators.required, SessionService.validatePassword]),
  });

  constructor(private session: SessionService) { }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  handleFormSubmit(event: Event) {
    event.preventDefault();
    if (this.loginForm.valid) {
       this.session.login(this.username!.value, this.password!.value);
    }
  }

}
