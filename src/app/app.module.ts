import { BrowserModule } from '@angular/platform-browser';
import { Injector, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { createCustomElement } from '@angular/elements';

import { LoginModule } from './login/login.module';
import { InvisalignPaymentsModule } from './invisalign-payments/invisalign-payments.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    LoginModule,
    InvisalignPaymentsModule,
  ],
  providers: [],
  // bootstrap: [AppComponent],
  entryComponents: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const pdfGenerator = createCustomElement(AppComponent, { injector });
    customElements.define('pdf-generator', pdfGenerator);
  }
  ngDoBootstrap() {}
}
