import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {AbstractControl} from '@angular/forms';

export interface UserInterface {
  username: string;
  password?: string;
}

export class UserModel implements UserInterface {
  constructor(public username: string) {}
}

const _username = 'doc';
const _password = 'pass';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _loggedInSubject = new BehaviorSubject(false);
  user?: UserInterface;
  loggedIn$ = this._loggedInSubject.asObservable();

  constructor() {}

  static validateUsername(control: AbstractControl) {
    return control.value !== _username ? { 'invalidUsername': { value: control.value } } : null;
  }

  static validatePassword(control: AbstractControl) {
    return control.value !== _password ? { 'invalidPassword': { value: control.value } } : null;
  }

  login(username: string, password: string) {
    this.user = new UserModel(username);
    this.validateCredentials(username, password)
      ? this._loggedInSubject.next(true)
      : this._loggedInSubject.next(false);
  }

  logout() {
    this.user = undefined;
    this._loggedInSubject.next(false);
  }

  private validateCredentials(username: string, password: string) {
    return username === _username && password === _password;
  }
}
