export enum TreatmentTypesEnum {
  comprehensive = 'Invisalign comprehensive',
  lightOneArch = 'Invisalign light - one arch',
  lightTwoArches = 'Invisalign light - two arches',
  expressOneArch = 'Invisalign express - one arch',
  expressTwoArches = 'Invisalign express - two arches',
}

export enum TreatmentPricesEnum {
  comprehensive = 11999,
  lightOneArch = 8299,
  lightTwoArches = 8999,
  expressOneArch = 6999,
  expressTwoArches = 7499,
}

export enum AmountOfDownpaymentsEnum {
  comprehensive = 3600,
  lightOneArch = 2700,
  lightTwoArches = 2800,
  expressOneArch = 2400,
  expressTwoArches = 2500,
}

export enum PriceOfCleancheckEnum {
  comprehensive = 1400,
  lightOneArch = 1400,
  lightTwoArches = 1400,
  expressOneArch = 1400,
  expressTwoArches = 1400,
}

export interface TreatmentTypeInterface {
  type: string;
  displayName: string;
  price: number;
  priceOfCleancheck: number;
  amountOfDownpayment: number;
  amountOfInstalments: number;
  instalments: number[];
  instalmentsSchedule?: any[];
  aligners?: number;
  weeks?: number;
}

export const TreatmentTypes: TreatmentTypeInterface[] = [
  {
    type: 'comprehensive',
    displayName: TreatmentTypesEnum.comprehensive,
    price: TreatmentPricesEnum.comprehensive,
    priceOfCleancheck: PriceOfCleancheckEnum.comprehensive,
    amountOfDownpayment: AmountOfDownpaymentsEnum.comprehensive,
    amountOfInstalments: 6,
    instalments: [1750, 1750, 1750, 1750],
    instalmentsSchedule: [
      {
        title: 'Pierwszy zestaw nakładek i pierwsza rata',
        date: {
          amount: 6,
          unit: 'weeks',
        },
        value: 1750
      },
      {
        title: 'Drugi zestaw nakładek i druga rata',
        date: {
          amount: (amountOfAligners: number) => {
            console.log(Math.round(0.25 * amountOfAligners) + 6);
            return Math.round(0.25 * amountOfAligners) + 6;
          },
          unit: 'weeks',
        },
        value: 1750
      },
      {
        title: 'Trzeci zestaw nakładek i trzecia rata',
        date: {
          amount: (amountOfAligners: number) => {
            console.log(Math.round(0.5 * amountOfAligners) + 6);
            return Math.round(0.5 * amountOfAligners) + 6;
          },
          unit: 'weeks',
        },
        value: 1750
      },
      {
        title: 'Czwarty zestaw nakładek i czwarta rata',
        date: {
          amount: (amountOfAligners: number) => {
            console.log(Math.round(0.75 * amountOfAligners) + 6);
            return Math.round(0.75 * amountOfAligners) + 6;
          },
          unit: 'weeks',
        },
        value: 1750
      }
    ]
  },
  {
    type: 'lightOneArch',
    displayName: TreatmentTypesEnum.lightOneArch,
    price: TreatmentPricesEnum.lightOneArch,
    priceOfCleancheck: PriceOfCleancheckEnum.lightOneArch,
    amountOfDownpayment: AmountOfDownpaymentsEnum.lightOneArch,
    amountOfInstalments: 5,
    aligners: 14,
    weeks: 20,
    instalments: [1400, 1400, 1400],
    instalmentsSchedule: [
      {
        title: 'Pierwszy zestaw nakładek i pierwsza rata',
        date: {
          amount: 6,
          unit: 'weeks',
        },
        value: 1400
      },
      {
        title: 'Drugi zestaw nakładek i druga rata',
        date: {
          amount: 11,
          unit: 'weeks',
        },
        value: 1400
      },
      {
        title: 'Trzeci zestaw nakładek i trzecia rata',
        date: {
          amount: 16,
          unit: 'weeks',
        },
        value: 1400
      }
    ]
  },
  {
    type: 'lightTwoArches',
    displayName: TreatmentTypesEnum.lightTwoArches,
    price: TreatmentPricesEnum.lightTwoArches,
    priceOfCleancheck: PriceOfCleancheckEnum.lightTwoArches,
    amountOfDownpayment: AmountOfDownpaymentsEnum.lightTwoArches,
    amountOfInstalments: 5,
    aligners: 14,
    weeks: 20,
    instalments: [1600, 1600, 1600],
    instalmentsSchedule: [
      {
        title: 'Pierwszy zestaw nakładek i pierwsza rata',
        date: {
          amount: 6,
          unit: 'weeks',
        },
        value: 1600
      },
      {
        title: 'Drugi zestaw nakładek i druga rata',
        date: {
          amount: 11,
          unit: 'weeks',
        },
        value: 1600
      },
      {
        title: 'Trzeci zestaw nakładek i trzecia rata',
        date: {
          amount: 16,
          unit: 'weeks',
        },
        value: 1600
      }
    ]
  },
  {
    type: 'expressOneArch',
    displayName: TreatmentTypesEnum.expressOneArch,
    price: TreatmentPricesEnum.expressOneArch,
    priceOfCleancheck: PriceOfCleancheckEnum.expressOneArch,
    amountOfDownpayment: AmountOfDownpaymentsEnum.expressOneArch,
    amountOfInstalments: 4,
    aligners: 7,
    weeks: 13,
    instalments: [1600, 1600],
    instalmentsSchedule: [
      {
        title: 'Pierwszy zestaw nakładek i pierwsza rata',
        date: {
          amount: 6,
          unit: 'weeks',
        },
        value: 1600
      },
      {
        title: 'Drugi zestaw nakładek i druga rata',
        date: {
          amount: 11,
          unit: 'weeks',
        },
        value: 1600
      }
    ]
  },
  {
    type: 'expressTwoArches',
    displayName: TreatmentTypesEnum.expressTwoArches,
    price: TreatmentPricesEnum.expressTwoArches,
    priceOfCleancheck: PriceOfCleancheckEnum.expressTwoArches,
    amountOfDownpayment: AmountOfDownpaymentsEnum.expressTwoArches,
    amountOfInstalments: 4,
    aligners: 7,
    weeks: 13,
    instalments: [1800, 1800],
    instalmentsSchedule: [
      {
        title: 'Pierwszy zestaw nakładek i pierwsza rata',
        date: {
          amount: 6,
          unit: 'weeks',
        },
        value: 1800
      },
      {
        title: 'Drugi zestaw nakładek i druga rata',
        date: {
          amount: 11,
          unit: 'weeks',
        },
        value: 1800
      }
    ]
  },
];
